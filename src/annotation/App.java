package annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface FirstAnnotation {
    int val() default 10;
    String str() default "example";

}

public class App {
    public static void main(String[] args) {
    runTheApp();
    }

    private static void runTheApp() {

        try {
            Example ap = new Example();
            Class c = ap.getClass();
            Method m = c.getMethod("example");
            FirstAnnotation ann = m.getAnnotation(FirstAnnotation.class);
            System.out.println(ann.val() + " " + ann.str());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

}
