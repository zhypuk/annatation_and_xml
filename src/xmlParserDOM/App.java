package xmlParserDOM;


import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class App {
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        App app = new App();
        app.runTheApp();
    }

    private void runTheApp() throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        org.w3c.dom.Document doc = db.parse("src/xmlParserDOM/books.xml");
        visit(doc);
    }

    public static void visit(Node node) {
        NodeList list = node.getChildNodes();
        for (int i=0;i<list.getLength();i++){
            Node childNode = list.item(i);
            process(childNode);
            visit(childNode);
        }
    }

    public static void process(Node node){

        if (node instanceof Element){
            System.out.print(node.getNodeName());
            } else if (node instanceof Text){
            System.out.print("   " +node.getNodeValue());
        }
    }

}
