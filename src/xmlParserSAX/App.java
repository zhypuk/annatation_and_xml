package xmlParserSAX;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

public class App {
    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
        final String fileName = "src/xmlParserDOM/books.xml";
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        DefaultHandler handler = new DefaultHandler(){
            boolean name = false;
            String str = "";
            @Override
            public void startElement(String uri, String localname, String qName, Attributes attributes)throws  SAXException{
                if(qName.equalsIgnoreCase("NAME")){
                    name = true;
                    str = "name";
                } else if (qName.equalsIgnoreCase("AUTHOR")){
                    name = true;
                    str = "author";
                } else if (qName.equalsIgnoreCase("PAGES")){
                    name = true;
                    str = "pages";
                }
            }

            @Override
            public void characters (char ch[], int start, int length) throws SAXException{
                if (name && str.equals("name")){
                    System.out.println("Name: " + new String(ch, start, length));
                    name = false;
                } else  if (name && str.equals("author")){
                    System.out.println("Author: " + new String(ch, start, length));
                    name = false;
                } else  if (name && str.equals("pages")){
                    System.out.println("Pages: " + new String(ch, start, length));
                    name = false;
                }
            }
        };
    saxParser.parse(fileName,handler);

    }
}
